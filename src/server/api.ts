import { IncomingMessage, ServerResponse } from 'http';
import data from './data.csv';
import url from 'url';

export const MAX_X = Math.max(...data.map(p => p.x));
export const MAX_Y = Math.max(...data.map(p => p.y));

export const getPagedData = (page = 1, size = data.length) => data.slice((page - 1) * size, page * size);

export default async function(req: IncomingMessage, res: ServerResponse) {
    const { query } = url.parse(req.url, true);
    const page = query.page ? parseInt(query.page.toString(), 10) : 1;
    const size = query.size ? parseInt(query.size.toString(), 10) : data.length;

    // handle bad page number
    if (page < 1) {
        res.statusCode = 400;
        res.end(`Bad Request: page numbers cannot be less than 1`);

        return;
    }

    const slice = getPagedData(page, size);
    const json = JSON.stringify(slice);

    res.statusCode = 200;
    res.setHeader('content-type', 'text/json');
    res.setHeader('X-Total-Results', data.length);
    res.setHeader('cache-control', `max-age=${60 * 60}`);
    res.end(json);
}
