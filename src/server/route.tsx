import React from 'react';
import { renderToString } from 'react-dom/server';
import { ServerStyleSheet } from 'styled-components';
import api, { getPagedData } from './api';
import App from '../components/App';

const scriptTagJSON = (js: object): string => JSON.stringify(js).replace(/<\/script>/gi, '<\\/script>');

export default async (req, res) => {
    const html = getHtml();

    res.statusCode = 200;
    res.setHeader('content-type', 'text/html');
    res.end(html);
};

export const getHtml = () => {
    const props = {
        points: getPagedData(1, 100),
    };
    const component = <App {...props} />;
    const sheet = new ServerStyleSheet();
    const content = renderToString(sheet.collectStyles(component));
    const styles = sheet.getStyleTags();

    const html = `
        <!doctype html>
        <html lang="en-GB">
            <head>
                <script defer src="/client.js"></script>
                <style>* { box-sizing: border-box; } html, body { height: 100%; margin: 0; }</style>
                ${styles}
            </head>
            <body>
                <div id="app">${content}</div>

                <script>
                    window.__PROPS__ = ${scriptTagJSON(props)};
                </script>
            </body>
        </html>`;

    return html;
};
