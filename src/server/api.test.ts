import api from './api';
import data from './data.csv';
import { URLSearchParams } from 'url';

jest.mock('./data.csv', () =>
    Array(5)
        .fill(0)
        .map((_, i) => ({ x: i, y: i })),
);

const getUrl = ({ page, size }: { page?: number; size?: number } = {}) => {
    const query = new URLSearchParams();
    if (page !== undefined) query.set('page', page.toString());
    if (size !== undefined) query.set('size', size.toString());

    return `/data/api?${query}`;
};

describe('api', () => {
    let req, res;

    beforeEach(() => {
        req = { url: getUrl() };
        res = { statusCode: undefined, setHeader: jest.fn(), end: jest.fn() };
    });

    test('sends all data if no querystring is provided', async () => {
        // ARRANGE
        req.url = '/data/api';

        // ACT
        await api(req, res);

        // ASSERT
        expect(res.statusCode).toBe(200);
        expect(res.setHeader).toHaveBeenCalledWith('content-type', 'text/json');
        expect(res.setHeader).toHaveBeenCalledWith('cache-control', expect.stringMatching(/^max-age=/));
        expect(res.end).toHaveBeenCalledWith(JSON.stringify(data));
    });

    test('sends subset of data if `page` and `size` querystring is provided', async () => {
        // ARRANGE
        req.url = getUrl({ page: 2, size: 2 });

        // ACT
        await api(req, res);

        // ASSERT
        expect(res.statusCode).toBe(200);
        expect(res.setHeader).toHaveBeenCalledWith('content-type', 'text/json');
        expect(res.setHeader).toHaveBeenCalledWith('cache-control', expect.stringMatching(/^max-age=/));
        expect(res.end).toHaveBeenCalledWith(JSON.stringify(data.slice(2, 4)));
    });

    test('sends first page of data if only `size` querystring is provided', async () => {
        // ARRANGE
        req.url = getUrl({ size: 2 });

        // ACT
        await api(req, res);

        // ASSERT
        expect(res.statusCode).toBe(200);
        expect(res.setHeader).toHaveBeenCalledWith('content-type', 'text/json');
        expect(res.setHeader).toHaveBeenCalledWith('cache-control', expect.stringMatching(/^max-age=/));
        expect(res.end).toHaveBeenCalledWith(JSON.stringify(data.slice(0, 2)));
    });

    test('sends Bad Request response when `page` is less than 1', async () => {
        // ARRANGE
        req.url = getUrl({ page: 0 });

        // ACT
        await api(req, res);

        // ASSERT
        expect(res.statusCode).toBe(400);
    });
});
