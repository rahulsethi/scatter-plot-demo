import route, { getHtml } from './route';

jest.mock('./data.csv', () =>
    Array(5)
        .fill(0)
        .map((_, i) => ({ x: i, y: i })),
);

describe('Main route middleware', () => {
    test('getHtml() matches snapshot', () => {
        // ACT
        const html = getHtml();

        // ASSERT
        expect(html).toMatchSnapshot();
    });

    test('html is sent to response', async () => {
        // ARRANGE
        const req = {};
        const res = {
            statusCode: undefined,
            setHeader: jest.fn(),
            end: jest.fn(),
        };

        // ACT
        await route(req, res);

        // ASSERT
        expect(res.statusCode).toBe(200);
        expect(res.setHeader).toBeCalledWith('content-type', 'text/html');
        expect(res.end).toBeCalledWith(getHtml());
    });
});
