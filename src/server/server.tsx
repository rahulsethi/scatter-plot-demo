import { createServer } from 'http';
import url from 'url';
import { createReadStream } from 'fs';
import route from './route';
import api from './api';

const scriptTagJSON = (js: object): string => JSON.stringify(js).replace(/<\/script>/gi, '<\\/script>');

export const server = createServer(async (req, res) => {
    const { pathname } = url.parse(req.url);
    switch (pathname) {
        case '/client.js':
            res.statusCode = 200;
            res.setHeader('content-type', 'application/javascript');
            createReadStream('./build/client.js').pipe(res);

            break;
        case '/':
            await route(req, res);

            break;
        case '/api/data':
            await api(req, res);

            break;
        default:
            res.statusCode = 404;
            res.end('Not Found');
    }
});
