import React from 'react';
import Page from './Page';
import { DataPoint, ScatterData, ScatterPoint } from '../types';

interface Props {
    points: DataPoint[];
}
interface State {
    data: ScatterData[];
}

class App extends React.Component<Props, State> {
    state = {
        data: this.transformPoints(this.props.points),
    };

    transformPoints(points: DataPoint[], pretransformedPoints: ScatterPoint[] = []): ScatterData[] {
        const offset = pretransformedPoints.length;
        const transformed = points.map((p, i) => ({ id: offset + i, ...p }));
        const concatenated = [...pretransformedPoints, ...transformed];
        return [{ id: 'points', data: concatenated }];
    }
    /*
        naive heuristic to estimate the optimal page size to
            1) load over the network, and
            2) render on screen
    */
    estimateOptimalPageSize() {
        const area = window.innerWidth * window.innerHeight;
        return Math.round(area / 4000 * window.devicePixelRatio);
    }

    stopLoading?: boolean;
    componentDidMount() {
        (async () => {
            const size = this.estimateOptimalPageSize();
            let page = 1;
            while (true) {
                const res = await fetch(`/api/data?page=${page}&size=${size}`);
                const points: DataPoint[] = await res.json();

                if (this.stopLoading) break;

                this.setState(state => ({
                    data: this.transformPoints(points, page === 1 ? [] : state.data[0].data),
                }));

                if (points.length < size) break;

                page++;
            }
        })();
    }
    componentWillUnmount() {
        this.stopLoading = true;
    }

    render() {
        return <Page data={this.state.data} />;
    }
}

export default App;
