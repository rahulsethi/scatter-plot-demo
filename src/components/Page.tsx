import React from 'react';
import styled from 'styled-components';
import { ResponsiveScatterPlotCanvas, ScatterPlot } from '@nivo/scatterplot';
import { ScatterData } from '../types';

const Outer = styled.div`
    * {
        font-family: 'Helvetica Neue', 'Segoe UI';
    }
    height: 100vh;

    display: flex;
    justify-content: center;
    align-items: center;

    overflow: hidden;
`;

// satsify server rendering
const ServerScatterPlot = props => (
    <div style={{ width: '100%', height: '100%' }}>
        <ScatterPlot width={800} height={600} {...props} />
    </div>
);

const Plot = process['browser'] ? ResponsiveScatterPlotCanvas : ServerScatterPlot;

interface Props {
    data: ScatterData[];
}

export const Page = ({ data, ...props }: Props) => (
    <Outer {...props}>
        <Plot data={data} symbolSize={3} colors="red" margin={{ top: 40, right: 40, bottom: 40, left: 80 }} />
    </Outer>
);

export default Page;
