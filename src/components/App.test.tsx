import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import App from './App';

Enzyme.configure({ adapter: new Adapter() });

describe('App component', () => {
    test('App component matches snapshot', () => {
        // ARRANGE
        const points = Array(5)
            .fill(0)
            .map((_, i) => ({ x: i, y: i }));

        // ACT
        const wrapper = shallow(<App points={points} />);

        // ASSERT
        expect(wrapper).toMatchSnapshot();
    });
});

describe('transformPoints()', () => {
    test('maps data into correct shape', () => {
        // ARRANGE
        const points = [{ x: 1, y: 2 }, { x: 3, y: 4 }];

        // ACT
        const transformed = App.prototype.transformPoints(points);

        // ASSERT
        expect(transformed).toEqual([
            {
                id: expect.stringContaining(''),
                data: [{ id: 0, x: 1, y: 2 }, { id: 1, x: 3, y: 4 }],
            },
        ]);
    });

    test('prepends a previously transformed array', () => {
        // ARRANGE
        const previous = [{ id: 0, x: 1, y: 2 }, { id: 1, x: 3, y: 4 }];
        const points = [{ x: 5, y: 6 }, { x: 7, y: 8 }];

        // ACT
        const transformed = App.prototype.transformPoints(points, previous);

        // ASSERT
        expect(transformed).toEqual([
            {
                id: expect.stringContaining(''),
                data: [{ id: 0, x: 1, y: 2 }, { id: 1, x: 3, y: 4 }, { id: 2, x: 5, y: 6 }, { id: 3, x: 7, y: 8 }],
            },
        ]);
    });
});
