import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({ adapter: new Adapter() });

describe('Page component', () => {
    const { browser } = process as any;

    afterEach(() => {
        Object.assign(process, { browser });
        jest.resetModules();
    });

    test('Page component matches snapshot on server', () => {
        // ARRANGE
        process['browser'] = false;
        const Page = require('./Page').default;
        const data = [
            {
                id: expect.stringContaining(''),
                data: [{ id: 0, x: 1, y: 2 }, { id: 1, x: 3, y: 4 }],
            },
        ];

        // ACT
        const wrapper = shallow(<Page data={data} />);

        // ASSERT
        expect(wrapper).toMatchSnapshot();
    });

    test('Page component matches snapshot in browser', () => {
        // ARRANGE
        process['browser'] = true;
        const Page = require('./Page').default;
        const data = [
            {
                id: expect.stringContaining(''),
                data: [{ id: 0, x: 1, y: 2 }, { id: 1, x: 3, y: 4 }],
            },
        ];

        // ACT
        const wrapper = shallow(<Page data={data} />);

        // ASSERT
        expect(wrapper).toMatchSnapshot();
    });
});
