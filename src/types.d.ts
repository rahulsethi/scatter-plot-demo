export interface DataPoint {
    x: number;
    y: number;
}

export interface ScatterData {
    id: string | number;
    data: ScatterPoint[];
}

export interface ScatterPoint {
    id: string | number;
    x: number;
    y: number;
}
