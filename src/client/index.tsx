import React from 'react';
import { render } from 'react-dom';
import App from '../components/App';

const component = <App {...window['__PROPS__'] || {}} />;

// explicit render, not hyrdate
render(component, document.querySelector('#app'));
