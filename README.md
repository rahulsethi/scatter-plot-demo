### Scatter Plot Demo

This is a simple web-app loaded by a Node.js server that plots the data in `src/server/data.csv` on a scatter chart.

The app is built using React and style-components, is server rendered, and is enchanced on the client.

The chart component(s) come from `@nivo`.

The server render includes a subset of the data to display something to the user as quick as possible. The rest of the data is lazy-loaded in chunks and rendered as each chunk comes in.

A naive heuristic is used to get the optimal size of the chunks using the viewport dimensions and the device pixel ratio.
